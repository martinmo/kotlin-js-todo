package todo

import kotlinx.html.*
import kotlinx.html.js.onBlurFunction
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.onSubmitFunction
import org.w3c.dom.HTMLInputElement
import kotlin.browser.document

object Views {

    fun FlowContent.renderTodoList(state: AppState, showDone: Boolean, update: (Command) -> Unit) {
        ul(classes = "list-group") {
            state.listTodos(showDone).forEach { todo ->
                li(classes = "list-group-item checkbox form-inline") {

                    div(classes = "checkbox") {
                        label {
                            checkBoxInput {
                                checked = todo.done
                                onChangeFunction = {
                                    update(Command.Companion.Toggle(todo))
                                }
                            }
                        }
                    }

                    textInput(classes = "form-control") {
                        style = "border:none"
                        placeholder = "description"
                        value = todo.description
                        id = todo.id.toString()

                        onBlurFunction = {
                            val newDescription = Views.getInputValueById(todo.id.toString())
                            update(Command.Companion.ChangeDescription(todo, newDescription))
                        }
                    }

                    button(classes = "pull-right btn") {
                        onClickFunction = { update(Command.Remove(todo)) }
                        span(classes = "glyphicon glyphicon-trash")
                    }
                }
            }
        }
    }

    fun FlowContent.renderShowDone(showDone: Boolean, toggle: () -> Unit) {
        div("panel panel-default") {
            div("panel-body form-inline") {
                div("checkbox") {
                    label {
                        checkBoxInput {
                            checked = showDone
                            onChangeFunction = {
                                toggle()
                            }
                        }
                        +"show done"
                    }
                }
            }
        }
    }

    fun FlowContent.renderCreateForm(update: (Command) -> Unit) {
        form(classes = "form-inline") {

            div("form-group") {
                textInput(classes = "form-control") {
                    id = "new"
                    required = true
                }

                button(classes = "btn", type = ButtonType.submit) {
                    span(classes = "glyphicon glyphicon-plus")
                }
            }

            onSubmitFunction = { e ->
                e.preventDefault()
                val newDescription = getInputValueById("new")
                update(Command.Add(Todo(newDescription)))
            }
        }
    }

    fun FlowContent.renderHistorySelection(history: List<HistoricState>, rollback: (HistoricState) -> Unit) {
        div {
            h2 { +"History" }
            ul {
                history.forEach { historicEntry ->
                    li {
                        button(classes = "btn") {
                            span("glyphicon glyphicon-backward")
                            onClickFunction = {
                                rollback(historicEntry)
                            }
                        }
                        span {
                            style = "font-weight:bold;"
                            +(" " + historicEntry.description + " on ")
                        }
                        span {
                            +historicEntry.date.toString()
                        }
                    }
                }
            }
        }
    }


    fun getInputValueById(id: String): String = (document.getElementById(id)!! as HTMLInputElement).value
}