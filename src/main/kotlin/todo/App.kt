package todo

import todo.Views.renderCreateForm
import todo.Views.renderHistorySelection
import todo.Views.renderShowDone
import todo.Views.renderTodoList
import kotlinx.html.DIV
import kotlinx.html.div
import kotlinx.html.dom.create
import kotlinx.html.h1
import org.w3c.dom.asList
import kotlin.browser.document

class App {
    var currentState = AppState()
    var showDone = false
    var historicStates: List<HistoricState> = emptyList()

    val updater: (Command) -> Unit = { command ->
        historicStates += HistoricState(currentState, command.description)
        currentState = currentState.reduce(command)
        render()
    }

    fun toggleShowDone() {
        showDone = !showDone
        render()
    }

    fun revertTo(historicState: HistoricState) {
        historicStates = historicStates.filter { it.date.getTime() < historicState.date.getTime() }
        currentState = historicState.state
        render()
    }

    fun render() {
        replaceBody {
            h1 { +"Todos" }

            renderCreateForm(updater)

            renderTodoList(currentState, showDone, updater)

            renderShowDone(showDone, { toggleShowDone() })

            renderHistorySelection(historicStates, { revertTo(it) })
        }
    }

    private fun replaceBody(init: DIV.() -> Unit) {
        val body = document.body!!

        body.childNodes.asList().forEach {
            body.removeChild(it)
        }

        val newBody = document.create.div("container", init)
        body.append(newBody)
    }
}
