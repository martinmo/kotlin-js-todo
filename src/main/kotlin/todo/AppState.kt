package todo

import kotlin.js.Date

data class AppState(private val todos: List<Todo> = emptyList()) {

    fun listTodos(showDone: Boolean) = if (showDone) todos else todos.filter { !it.done }

    fun reduce(cmd: Command): AppState = when (cmd) {
        is Command.Add -> this.copy(todos = todos + cmd.todo)
        is Command.Update -> this.copy(todos = todos.replace(cmd.todo, { it.id == cmd.todo.id }))
        is Command.Remove -> this.copy(todos = todos.filter { it.id != cmd.todo.id })
    }
}

data class Todo(val description: String, val done: Boolean = false, val id: Long = Id.next())

sealed class Command(val description: String) {
    data class Add(val todo: Todo) : Command("Add")
    data class Update(val todo: Todo, val desc: String) : Command(desc)
    data class Remove(val todo: Todo) : Command("Remove")

    companion object {
        fun Toggle(old: Todo) = Update(old.copy(done = !old.done), "Toggle")
        fun ChangeDescription(old: Todo, description: String) = Update(old.copy(description = description), "Edit")
    }
}

object Id {
    var id: Long = 0
    fun next() = id++
}

data class HistoricState(val state: AppState, val description: String, val date: Date = Date())

fun <T> List<T>.replace(t: T, predicate: (T) -> Boolean): List<T> = this.map {
    if (predicate(it)) t else it
}