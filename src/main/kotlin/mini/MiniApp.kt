package mini

import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.onBlurFunction
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.onSubmitFunction
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.asList
import kotlin.browser.document
import kotlin.js.Date

class MiniApp {
    class Todo(var description: String, var done: Boolean = false, val id: String = Date().getTime().toString())

    val todos: MutableList<Todo> = mutableListOf()
    var showDone = false

    fun render() {

        replaceBody {
            h1 { +"Todos" }

            form {
                textInput {
                    id = "new"
                    required = true
                }
                button(type = ButtonType.submit) {
                    +"create"
                }

                onSubmitFunction = { e ->
                    e.preventDefault()
                    todos.add(Todo(getInputValueById("new")))
                    render()
                }
            }

            ul {
                todos.filter { if (showDone) true else !it.done }.forEach { todo ->
                    li {
                        checkBoxInput {
                            checked = todo.done
                            onChangeFunction = {
                                todo.done = !todo.done
                                render()
                            }
                        }

                        textInput {
                            value = todo.description
                            id = todo.id
                            onBlurFunction = {
                                todo.description = getInputValueById(todo.id)
                                render()
                            }
                        }

                        button {
                            +"remove"
                            onClickFunction = {
                                todos.removeAll { it.id == todo.id }
                                render()
                            }
                        }
                    }
                }
            }

            div {
                checkBoxInput {
                    checked = showDone
                    onChangeFunction = {
                        showDone = !showDone
                        render()
                    }
                }
                +"show done"
            }

        }

    }

    private fun replaceBody(init: DIV.() -> Unit) {
        val body = document.body!!

        body.childNodes.asList().forEach {
            body.removeChild(it)
        }

        val newBody = document.create.div("container", init)
        body.append(newBody)
    }

    fun getInputValueById(id: String): String = (document.getElementById(id)!! as HTMLInputElement).value
}
