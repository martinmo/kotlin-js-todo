import mini.MiniApp
import todo.App
import todo.Command
import todo.Todo
import kotlin.browser.document
import kotlin.browser.window

fun main(args: Array<String>) {
    window.onload = {

        if (document.documentURI.contains("mini")) {
            val app = MiniApp()
            app.todos += MiniApp.Todo("Item 1")
            app.render()
        } else {
            val app = App()
            app.updater(Command.Add(Todo("Make it good")))
            app.updater(Command.Add(Todo("Make it bad")))
        }
    }
}