Todo with Kotlin.js 
========================

`mvn compile`

Full Version
---
- open `target/classes/todos.html` 
- source in 'src/main/kotlin/todo/*'
- Features: immutable state, history and revert, bootstrap CSS

Light Version
---
 - open `target/classes/mini.html`
 - source in 'src/main/kotlin/mini/MiniApp.kt'
 - Features: 1 file, 100 lines of Code